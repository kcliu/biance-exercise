import React, { Component } from 'react';
import { AutoSizer, List as ReactVirtualizedList } from 'react-virtualized';
import { humanizeTimeFormat } from '../../utils/time';

import styled from 'styled-components';

const Container = styled.div`
  height: 800px;
  width: 40%;
  margin-left: 50px;
`;

const TableHeader = styled.div`
  display: flex;
  margin-right: 15px; /* for shift width of scroll bar */
  justify-content: flex-start;
  div {
    flex: 1;
  }
`;

const TableRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 100%;
  font-size: 12px;
  background-color: rgba(255, 255, 255, 0.06);

  &:hover {
    background-color: rgba(255, 255, 255, 0.15);
  }
`;

const Price = styled.span`
  flex: 1;
`;

const Qty = styled.span`
  flex: 1;
`;

const Time = styled.span`
  flex: 1;
`;


class TradeHistory extends Component {

  //renderer
  renderHeader = () => {
    return (
      <TableHeader>
        <div>Price</div>
        <div>Amount</div>
        <div>Time</div>
      </TableHeader>
    )
  }

  renderRow = ({key, index, style}) => {
    const { tradeList } = this.props;
    const {symbol, price, qty, time} = tradeList[index];

    if (tradeList.length <= 0) return;

    return (
      <TableRow key={key} style={style}>
        <Price>{price}</Price>
        <Qty>{qty}</Qty>
        <Time>{humanizeTimeFormat(time)}</Time>
      </TableRow>
    )
  }

  render() {
    const { tradeList } = this.props;

    return (
      <Container>
        {this.renderHeader()}
        <AutoSizer>
          {({ width, height }) => (
            <ReactVirtualizedList
              width={width}
              height={height}
              rowHeight={40}
              rowRenderer={this.renderRow}
              rowCount={tradeList.length}
            />
          )}
        </AutoSizer>
      </Container>
    )
  }
}

export default TradeHistory;
