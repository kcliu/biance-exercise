import React from 'react';
import AbstractOrder from './AbstractOrder';

import styled from 'styled-components';
import { TableRow } from './BidOrder';

const FillAsk = styled.div`
  position: absolute;
  width: 100%;
  height: 40px;
  right: 0;
  background-color: rgb(215,84, 85);
  opacity: .5;
`;

class AskOrder extends AbstractOrder {

  render() {
    return (
      <TableRow {...this.props}>
        <div>{this.props.price}</div>
        <div>{this.props.quantity}</div>
        <FillAsk style={{width: `${this.getPercentage()}%`}}/>
      </TableRow>
    );
  }
}

export default AskOrder;
