import React, { Component, Fragment } from 'react';
import { AutoSizer, List as ReactVirtualizedList } from 'react-virtualized';

import AskOrder from './AskOrder';
import BidOrder from './BidOrder';

import styled from 'styled-components';

const Container = styled.div`
  flex: 1;
  height: 800px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const TableHeader = styled.div`
  display: flex;
  align-items: center;
  margin-right: 15px; /* for shift width of scroll bar */
  div {
    flex: 1;
  }
`;

const ChartWrapper = styled.div`
  flex: 1;
  height: 100%;
  margin: 0 20px;
`;

class OrderBook extends Component {

  sumQuantities(orders) {
    return orders.reduce((total, order) => total + order.quantity, 0);
  }

  transformOrders(orders, maxCumulative) {
    let cumulative = 0;
    return orders.map((order, index) => {
      order.cumulative = (cumulative += order.quantity);
      order.maxCumulative = maxCumulative;
      return order;
    }).sort();
  }

  //renderer

  renderHeader = () => {
    return (
      <TableHeader>
        <div>Price</div>
        <div>Amount</div>
      </TableHeader>
    )
  }

  renderAskRow = ({key, index, style}) => {
    const {askOrders, bidOrders} = this.props;

    const totalAsks = this.sumQuantities(askOrders);
    const totalBids = this.sumQuantities(bidOrders);

    const maxCumulative = Math.max(totalAsks, totalBids);

    // Deep copy and sort orders
    const updatedAskOrders = this.transformOrders(askOrders.sort((a, b) => a.price > b.price), maxCumulative).reverse(); // ascending order

    return (
      <AskOrder key={`ask-${key}`} style={style} {...updatedAskOrders[index]} />
    )
  }

  renderBidRow = ({key, index, style}) => {
    const {askOrders, bidOrders} = this.props;

    const totalAsks = this.sumQuantities(askOrders);
    const totalBids = this.sumQuantities(bidOrders);

    const maxCumulative = Math.max(totalAsks, totalBids);

    // Deep copy and sort orders
    const updatedBidOrders = this.transformOrders(bidOrders.sort((a, b) => a.price < b.price), maxCumulative); // descending order

    return (
      <BidOrder key={`bid-${key}`} style={style} {...updatedBidOrders[index]} />
    )
  }

  renderAskChart = () => {
    const { askOrders } = this.props;
    return (
      <ChartWrapper>
        {this.renderHeader()}
        <AutoSizer>
          {({ width, height }) => (
            <ReactVirtualizedList
            width={width}
            height={height}
            rowHeight={40}
            rowRenderer={this.renderAskRow}
            rowCount={askOrders.length}
            scrollToIndex={askOrders.length}
            />
          )}
        </AutoSizer>
      </ChartWrapper>
    )
  }

  renderBidChart = () => {
    const { bidOrders } = this.props;
    return (
      <ChartWrapper>
        {this.renderHeader()}
        <AutoSizer>
          {({ width, height }) => (
            <ReactVirtualizedList
            width={width}
            height={height}
            rowHeight={40}
            rowRenderer={this.renderBidRow}
            rowCount={bidOrders.length}
          />
          )}
        </AutoSizer>
      </ChartWrapper>
    )
  }

  render() {
    const  {askOrders, bidOrders} = this.props;

    return (
      <Container>
        {this.renderAskChart()}
        {this.renderBidChart()}
      </Container>
    );
  }
}

export default OrderBook;
