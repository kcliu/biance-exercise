import React from 'react';
import AbstractOrder from './AbstractOrder';

import styled from 'styled-components';

export const TableRow = styled.div`
  position: relative;
  height: 40px;
  display: flex;
  font-size: 12px;
  background-color: rgba(255, 255, 255, 0.06);

  div {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: flex-start;
  }

  &:hover {
    background-color: rgba(255, 255, 255, 0.15);
  }
`;

const FillBid = styled.div`
  position: absolute;
  width: 100%;
  height: 40px;
  right: 0;
  background-color: rgb(134, 193, 102);
  opacity: .5;
`;

class BidOrder extends AbstractOrder {

  render() {
    return (
      <TableRow {...this.props}>
        <div>{this.props.price}</div>
        <div>{this.props.quantity}</div>
        <FillBid style={{width: `${this.getPercentage()}%`}}/>
      </TableRow>
    );
  }
}

export default BidOrder;
