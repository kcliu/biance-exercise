// @flow

const CONTENT_TYPE = 'Content-Type';
const CONTENT_TYPE_JSON = 'application/json';

function serverAPI(url: string, options?: RequestOptions): Promise<any> {
  return fetch(url, options)
    .then(function(response) {
      if (response.headers.get(CONTENT_TYPE) === CONTENT_TYPE_JSON) {
        return response.json();
      }
      return response.text();
    })
    .catch((err) => {
      throw err;
    })

}

export default serverAPI;
