import { humanizeTimeFormat } from '../time';

describe('humanizeTimeFormat', () => {
  it('shows the human readable format', () => {
    expect(humanizeTimeFormat(1538909339978)).toBe('18:48:59');
  });
});
