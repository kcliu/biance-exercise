// @flow

import { format } from 'date-fns';

/**
 * get readable time format
 *
 * @param {number} time the unix time
 * @returns {string} e.g. '17:30:02'
 */
export function humanizeTimeFormat(time: number): string {
  return format(time, 'HH:mm:ss');
}
