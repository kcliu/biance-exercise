// @flow

import { combineReducers } from 'redux'
import trade from './trade';
import home from './home';

export default combineReducers({
  home,
  trade,
})
