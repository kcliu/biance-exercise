//@flow

import serverAPI from '../apis/serverAPI';

export const GET_PRICE = 'trade/GET_PRICE';

type HomeState = {
  priceList: [],
}
type ActionType = {type: string, res: any };

const initialState: HomeState = {
  priceList: [],
}

export default (state: HomeState = initialState, action: ActionType) => {
  switch (action.type) {
    case GET_PRICE: {

      const { res: payload } = action;
      return {
        ...state,
        priceList: payload,
      }
    }

    default:
      return state
  }
}

export const getPrices = () => {
  return (dispatch: any) => {
    serverAPI('https://api.binance.com/api/v3/ticker/price')
      .then(res => dispatch({type: GET_PRICE, res}))
  }
}
