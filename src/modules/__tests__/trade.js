import trade, { initState } from '../trade';

import { getDepthAction } from '../mockData/trade';

describe('Reducer: trade', () => {
  it('state change: load trade data', () => {
    expect(trade(initState, getDepthAction)).toMatchSnapshot();
  });
});
