//@flow

import serverAPI from '../apis/serverAPI';

export const GET_TRADE = 'trade/GET_TRADE';
export const GET_DEPTH = 'trade/GET_DEPTH';

type TradeState = {
  tradeList: [],
  depthMap: {
    asksList: string[],
    bidsList: string[],
  },
};

type ActionType = {type: string, res: any };

export const initialState: TradeState = {
  tradeList: [],
  depthMap: {
    asksList: [],
    bidsList: [],
  },
}

export default (state: TradeState = initialState, action: ActionType) => {
  switch (action.type) {
    case GET_TRADE: {
      const { res: payload } = action;

      return {
        ...state,
        tradeList: payload,
      }
    }

    case GET_DEPTH: {
      const { res: {asks, bids} } = action;
      return {
        ...state,
        depthMap: {
          asksList: asks,
          bidsList: bids,
        },
      }
    }

    default:
      return state
  }
}

export const getTrades = (symbol:string) => {
  return (dispatch: any) => {
    serverAPI(`https://api.binance.com/api/v1/trades?symbol=${symbol}&limit=50`)
      .then(res => dispatch({type: GET_TRADE, res}))
  }
}

export const getDepth = (symbol:string) => {
  return (dispatch: any) => {
    serverAPI(`https://api.binance.com/api/v1/depth?symbol=${symbol}&limit=50`)
      .then(res => dispatch({type: GET_DEPTH, res}))
  }
}
