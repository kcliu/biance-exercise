//@flow

import React from 'react';
import { Route, Link } from 'react-router-dom';
import Home from '../home';
import Trade from '../trade';

import styled from 'styled-components';

const AppContainer = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
`;

const Nav = styled.nav`
  display: flex;
  align-items: center;
  justify-content: left;
  padding: 20px 0;
  border-bottom: 1px solid rgba(255, 255, 255, 0.2);
  > a {
    display: flex;
    justify-content: center;
    align-items: center;

    padding: 0 30px;
    transition: color 0.1s;
    cursor: pointer;
    user-select: none;
    color: white;
    &:hover {
      color: #24acff;
    }

    &:active {
      color: #006faf;
    }
  }
`;

const App = () => (
  <AppContainer>
    <Nav>
      <Link to="/">Home</Link>
    </Nav>

    <main>
      <Route exact path="/" component={Home} />
      <Route path="/trade/:symbol" component={Trade} />
    </main>
  </AppContainer>
)

export default App
