//@flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { AutoSizer, List as ReactVirtualizedList } from 'react-virtualized';
import { connect } from 'react-redux'

import { getPrices } from '../../modules/home';

// css
import styled from 'styled-components';

const Container = styled.div`
  height: 1000px;
  width: 100%;
  padding: 20px;
`;

const TableHeader = styled.div`
  display: flex;
  align-items: center;
  div {
    flex: 1;
  }
`;

const TableRow = styled.div`
  display: flex;
  align-items: center;
  padding: 20px;
  height: 100%;
  font-size: 12px;
  background-color: rgba(255, 255, 255, 0.06);

  &:hover {
    background-color: rgba(255, 255, 255, 0.15);
  }
`;

const Symbol = styled.div`
  flex: 1;
`;

const Price = styled.div`
  flex: 1;
`;

type Props = {
  dispatch: any,
  priceList: [],
}

class Home extends Component<Props> {

  componentDidMount() {
    this.props.dispatch(getPrices());
  }

  // -------------------------------------
  //   Renderer
  // -------------------------------------
  renderRow = ({key, index, style}) => {
    const { priceList } = this.props;
    const {symbol, price} = priceList[index];

    if (priceList.length <= 0) return;

    return (
      <Link key={key} to={`/trade/${symbol}`}>
        <TableRow style={style}>
            <Symbol>{symbol}</Symbol>
            <Price>{price}</Price>
        </TableRow>
      </Link>
    )
  }
  renderHeader = () => {
    return (
      <TableHeader>
        <div>Market Pairs</div>
        <div>Price</div>
      </TableHeader>
    )
  }

  render() {
    const { priceList } = this.props;

    return (
      <Container>
        {this.renderHeader()}
        <AutoSizer>
          {({ width, height }) => (
            <ReactVirtualizedList
              width={width}
              height={height}
              rowHeight={40}
              rowRenderer={this.renderRow}
              rowCount={priceList.length}
            />
          )}
        </AutoSizer>
      </Container>
    )
  }
}

const mapStateToProps = ({ home }) => ({
  priceList: home.priceList,
});

export default connect(mapStateToProps)(Home);
