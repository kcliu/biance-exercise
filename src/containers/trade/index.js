//@flow

import React, { Component } from 'react';
import { type Match } from 'react-router-dom';
import { connect } from 'react-redux'

import OrderBook from '../../components/OrderBook';
import TradeHistory from '../../components/TradeHistory';
import { getTrades, getDepth } from "../../modules/trade";
//css
import styled from 'styled-components';

const TradeContainer = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: center;
  padding: 20px;
`;

type Props = {
  // From router
  match: Match,
  // From connect()
  tradeList: [],
  depthMap: {
    asksList: string[],
    bidsList: string[],
  },
  dispatch: any,
}

class Trade extends Component<Props> {

  componentDidMount() {
    const { symbol } = this.props.match.params;

    this.props.dispatch(getTrades(symbol));
    this.props.dispatch(getDepth(symbol));
  }

  // -------------------------------------
  //   Renderer
  // -------------------------------------
  renderOrderBook() {
    const { depthMap: {asksList, bidsList} } = this.props;
    if (Object.keys(this.props.depthMap).length === 0) return;

    const askOrders = asksList.map(ask => ({
      price: +ask[0],
      quantity: +ask[1]
    }));

    const bidOrders = bidsList.map(bid => ({
      price: +bid[0],
      quantity: +bid[1]
    }));

    return (
      <OrderBook askOrders={askOrders} bidOrders={bidOrders} />
    )
  }

  renderTradeHistory() {
    const { tradeList } = this.props;

    return (
      <TradeHistory tradeList={tradeList}/>
    )
  }

  render() {
    return (
      <TradeContainer>
        {this.renderOrderBook()}
        {this.renderTradeHistory()}
      </TradeContainer>
    )
  }
}

const mapStateToProps = ({ trade:{tradeList, depthMap} }) => ({
  tradeList,
  depthMap,
});

export default connect(mapStateToProps)(Trade);
