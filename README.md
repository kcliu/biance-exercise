## Get started

to start:
```
yarn start
```

to run test:
```
yarn test
```

p.s.
Currently has
1. one unit test
2. one snapshot test

TODO:
1. introduce immutable.js
2. add storybook
3. test for components
4. eslint & prettier check when git commit


This boilerplate is built using [create-react-app](https://github.com/facebook/create-react-app) so you will want to read the User Guide for more goodies.
